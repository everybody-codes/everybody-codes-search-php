<?
define("NO_NAME_ERROR", "Please run me passing a --name arg!\n");
define("NO_RESULTS", "No results found!\n");

$nameArgIndex = array_search("--name", $argv);
$nameArg = $argv[$nameArgIndex+1];

if(!$nameArgIndex || !$nameArg) {
  exit(NO_NAME_ERROR);
}


$csvData = file_get_contents("./data/cameras-defb.csv");

$records = explode("\n", $csvData);

// Remove the header row
unset($records[0]);

$results = array();
foreach ( $records as $record ) {
   $fields = str_getcsv( $record, ";" );
   $name = $fields[0];
   if(count($fields) === 3 && strpos(strtolower($name), strtolower($nameArg)) !== false) {
     $id = substr(str_replace("UTR-CM-", "", $name), 0, 3);
     array_unshift($fields, $id);
     array_push($results, implode(" | ", $fields));
   }
}

foreach ( $results as $result ) {
  echo $result . "\n";
}

if(!count($results)) {
  echo NO_RESULTS;
}
?>
