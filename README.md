# everybody-codes-search-php

Ik heb dit script gemaakt als alternatief op het Node-script in https://gitlab.com/everybody-codes/everybody-codes-api.

De reden is simpel: het voelde niet goed om enkel JavaScript skills te tonen.

### De CLI gebruiken
`php search.php --name [name]`
